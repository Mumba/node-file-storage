# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.2.3] 23 Feb 2017
- Added `S3FileStorageService.getSignedUrl`.

## [0.2.2] 22 Dec 2016
- Added `ACL`, `ServerSideEncryption` and `StorageClass` to the copy part of the `putFile` operation.

## [0.2.1] 22 Dec 2016
- Added `ACL`, `ServerSideEncryption` and `StorageClass` to the copy part of the `moveFile` operation.

## [0.2.0] 22 Dec 2016
- Fixed problem in `moveFile`. 

## [0.1.1] 22 Nov 2016
- Initial release.
