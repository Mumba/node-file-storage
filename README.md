## Mumba File Storage

Interfaces for dealing with files.

## Installation

```sh
$ npm install --save mumba-file-storage
```

## S3 Example

```typescript
import {S3FileStorageService} from `mumba-file-storage`;
import {S3} from 'aws-sdk';

let tmpDir = __dirname + '/tmp';
let options = {
	Bucket: 'the-bucket',
	tmpDir: tmpDir
};

let s3Options = {
	tmpDir: tmpDir,
	accessKeyId: '',
	secretAccessKey: '',
	Bucket: 'the-bucket',
	ACL: 'private',
	ServerSideEncryption: 'AES256'
};

let awsS3 = new S3(s3Options);

let instance = new S3FileStorageService(options, awsS3);

instance.getFile('/path/to/object');

instance.putFile(`${tmpDir}/file`, '/path/to/object');

instance.moveFile('/path/to/original/file', '/path/to/new/location');
```

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm install
$ npm test
```

## People

The original author of _Mumba File Storage_ is [Scott Robinson](https://gitlab.com/SnareChops).

[List of all contributors](https://gitlab.com/Mumba/node-stream/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.
