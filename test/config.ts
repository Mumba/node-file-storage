/**
 * Test configuration.
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

let testConfig = {
	aws: {
		s3: {
			tmpDir: __dirname + '/../../../tmp',
			accessKeyId: '',
			secretAccessKey: '',
			Bucket: 'odyssey-encrypted',
			ACL: 'private',
			ServerSideEncryption: 'AES256'
		}
	}
};

export default testConfig;
