/**
 * AbstractFileStorageService tests.
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import * as fs from 'fs';
import {Readable} from 'stream';
import * as rimraf from 'rimraf';
import {AbstractFileStorageService, AbstractFileStorageServiceOptions} from '../../src/index';
import ReadableStream = NodeJS.ReadableStream;
import {F_OK} from 'constants';

export class TestAbstractFileStorageService extends AbstractFileStorageService {
	constructor(options: AbstractFileStorageServiceOptions) {
		super(options);
	}

	// Underlying method is protected. This exposes it for testing purposes
	public exposeTmpDir(): string {
		return this.getTempDir();
	}

	// Underlying method is protected. This exposes it for testing purposes
	public exposeSaveToTempDir(stream: ReadableStream, filename: string): Promise<string> {
		return this.saveToTempDir(stream, filename);
	}
}

describe('AbstractFileStorageService', () => {
	let instance: TestAbstractFileStorageService;

	beforeEach(() => {
		instance = new TestAbstractFileStorageService({ tmpDir: `${__dirname}/tmp` });
	});

	it('should throw if no temp directory is found or readable', () => {
		assert.throws(() => {
			new TestAbstractFileStorageService({ tmpDir: instance.exposeTmpDir() + '/trash' });
		});
		fs.mkdirSync(instance.exposeTmpDir() + '/trash');
		assert.doesNotThrow(() => {
			new TestAbstractFileStorageService({ tmpDir: instance.exposeTmpDir() + '/trash' });
		});
		rimraf.sync(instance.exposeTmpDir() + '/trash');
	});

	it('should error if `options.tmpDir` is missing in the constructor', () => {
		assert.throws(() => {
			new TestAbstractFileStorageService(void 0)
		}, /<options.tmpDir> required/);
	});

	it('should save a file to the temp dir', (done) => {
		function exists() {
			fs.accessSync(instance.exposeTmpDir() + '/test-file-save.txt', F_OK);
		}

		const stream = new Readable({ objectMode: true });

		stream['_read'] = () => {
			stream.push('abc123');
			stream.push(null);
		};

		instance.exposeSaveToTempDir(stream, 'test-file-save.txt')
			.then(() => {
				assert.doesNotThrow(exists);
				done();
			})
			.catch(done);
	});

	it('should cleanup a temp file', (done) => {
		function exists() {
			fs.accessSync(instance.exposeTmpDir() + '/test-file.txt', F_OK);
		}

		fs.writeFileSync(`${instance.exposeTmpDir()}/test-file.txt`, 'Test content');
		assert.doesNotThrow(exists);
		instance.cleanupTempFile('test-file.txt')
			.then(() => {
				assert.throws(exists);
				done();
			})
			.catch(done);
	});
});
