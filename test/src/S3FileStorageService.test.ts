/**
 * S3 service tests.
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const Readable = require('stream').Readable;
const TEST_FILE = __dirname + '/file.txt';

import * as fs from 'fs';
import * as assert from 'assert';
import * as AWS from 'aws-sdk';
import {S3} from 'aws-sdk';
import {S3FileStorageService, S3FileStorageServiceOptions} from "../../src/index";
import {Request} from 'aws-sdk';
import PutObjectRequest = S3.Types.PutObjectRequest;
import CopyObjectRequest = S3.Types.CopyObjectRequest;
import DeleteObjectRequest = S3.Types.DeleteObjectRequest;
import CopyObjectOutput = S3.Types.CopyObjectOutput;

describe('S3FileStorageService unit tests', () => {
	var options: S3FileStorageServiceOptions;
	let awsS3: AWS.S3;
	var instance: S3FileStorageService;

	beforeEach(() => {
		options = {
			Bucket: 'the-bucket',
			ACL: 'private',
			ServerSideEncryption: 'AES256',
			StorageClass: 'REDUCED_REDUNDANCY',
			tmpDir: __dirname + '/tmp'
		};
		awsS3 = Object.create(AWS.S3.prototype);
		instance = new S3FileStorageService(options, awsS3);

		if (fs.existsSync(TEST_FILE)) {
			fs.unlinkSync(TEST_FILE);
		}
	});

	it('should error if `options.Bucket` is missing in the constructor', () => {
		assert.throws(() => {
			new S3FileStorageService(<any>{ tmpDir: __dirname + '/tmp' }, awsS3)
		}, /<options.Bucket> required/);
	});

	it('should error if `awsS3` is missing in the constructor', () => {
		assert.throws(() => {
			new S3FileStorageService(options, void 0)
		}, /<awsS3> required/);
	});

	it('should get a file', (done) => {
		awsS3.getObject = () => {
			let stream = new Readable({ objectMode: true });

			stream._read = () => {
				stream.push('abc123');
				stream.push(null);
			};

			return <Request<any,any>>{
				createReadStream: () => stream
			};
		};

		instance.getFile('/path/to/object')
			.then((filePath) => {
				const result = fs.readFileSync(filePath);
				assert.equal(result, 'abc123', 'should be the file sent by the dummy stream');
				done();
			})
			.catch(done);
	});

	it('should put a file', (done) => {
		awsS3.upload = (params: PutObjectRequest, callback: Function) => {
			assert.strictEqual(params.Body instanceof Readable, true, 'should be a ReadableStream');
			assert.equal(params.ACL, options.ACL, 'should be the ACL option');
			assert.equal(params.ServerSideEncryption, options.ServerSideEncryption, 'should be the ServerSideEncryption option');
			assert.equal(params.StorageClass, options.StorageClass, 'should be the StorageClass option');
			callback();
			return <Request<any,any>>{};
		};

		const tempFilePath = __dirname + '/tmp/fakeFileForPut.txt';
		fs.writeFileSync(tempFilePath, 'xyz789');
		instance.putFile(tempFilePath, '/path/to/file')
			.then(done)
			.catch(done);
	});

	it('should move a file', (done) => {
		const originalFilePath = 'foo/bar.txt';
		let key = 'new/path/to/file';

		let copyFunc = (params: CopyObjectRequest, callback: Function) => {
			assert.equal(params.CopySource, options.Bucket + '/' + originalFilePath, 'should be the original file path with bucket');
			assert.equal(params.ACL, options.ACL, 'should be the ACL option');
			assert.equal(params.ServerSideEncryption, options.ServerSideEncryption, 'should be the ServerSideEncryption option');
			assert.equal(params.StorageClass, options.StorageClass, 'should be the StorageClass option');
			assert.equal(params.Key, key, 'should be the new Key');
			callback();
			return <Request<any,any>>{};
		};

		let deleteFunc = (params: DeleteObjectRequest, callback: Function) => {
			assert.equal(params.Key, originalFilePath, 'should be the original file path');
			callback();
			return <Request<any,any>>{};
		};

		awsS3.copyObject = <any>copyFunc;
		awsS3.deleteObject = <any>deleteFunc;

		instance.moveFile(originalFilePath, key)
			.then(done)
			.catch(done);
	});

	it('should get a signed URL', () => {
		let key = 'path/to/file';

		awsS3.getSignedUrl = (operation: string, params: any) => {
			assert.equal(operation, 'getObject', 'should be the operation');
			assert.equal(params.Bucket, options.Bucket, 'should be the bucket');
			assert.equal(params.Key, key, 'should be the key');

			return 'signed/url'
		};

		let url = instance.getSignedUrl(key);

		assert.equal(url, 'signed/url', 'should be the url');
	});
});
