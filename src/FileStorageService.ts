/**
 * FileStorageService.
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import ReadableStream = NodeJS.ReadableStream;

/**
 * Interface for all `FileStorageService`s to implement
 *
 * NOTE: `cleanupTempFile` is implemented in `AbstractedFileStorageService`
 * If you extend this class then you will not be required to implement it
 */
export interface FileStorageService {
	/**
	 * Gets a file from the storage.
	 *
	 * @param pathOrKey {string} - The key or path to the file.
	 * @returns {Promise<string>} - The file path to the temp file
	 */
	getFile(pathOrKey: string): Promise<string>;

	/**
	 * Puts a file in the storage.
	 *
	 * @param tempFilePath {string} - The path of the temp file to save
	 * @param pathOrKey {string}    - The AWS S3 key to save the file as
	 * @returns {Promise<void>}
	 */
	putFile(tempFilePath: string, pathOrKey: string): Promise<void>;

	/**
	 * Removes the file from the temp directory.
	 *
	 * @param filename {string} - Filename to remove
	 * @returns {Promise<void>}
	 */
	cleanupTempFile(filename: string): Promise<void>;

	/**
	 * Moves an existing file in the S3 bucket
	 *
	 * @param originalFilePath {string} - The path of the original file to move
	 * @param key {string}              - The AWS S3 key to move the file to
	 * @returns {Promise<void>}
	 */
	moveFile(originalFilePath: string, key: string): Promise<void>;

	/**
	 * Get a signed URL to download a file.
	 *
	 * @param filePath
	 * @returns {string}
	 */
	getSignedUrl(filePath: string): string;
}
