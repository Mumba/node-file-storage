/**
 * AbstractFileStorageService.
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as fs from 'fs';
import * as path from 'path';
import ReadableStream = NodeJS.ReadableStream;
import {F_OK} from 'constants';

export interface AbstractFileStorageServiceOptions {
	tmpDir: string;
}

export abstract class AbstractFileStorageService {

	constructor(protected options: AbstractFileStorageServiceOptions = <any>{}) {
		if (!options.tmpDir) {
			throw new Error('AbstractFileStorageServiceOptions: <options.tmpDir> required.');
		}

		try {
			fs.accessSync(options.tmpDir, F_OK);
		}
		catch (e) {
			throw new Error('AbstractFileStorageService: <options.tmpDir> not found or readable.');
		}
	}

	/**
	 * Gets the temp directory path
	 *
	 * @returns {string}
	 */
	protected getTempDir(): string {
		return this.options.tmpDir;
	}

	/**
	 * Saves a file to the temp dir from a stream
	 *
	 * @param stream {ReadableStream} - The stream containing the file
	 * @param filename {string}       - Filename to save the temp file as
	 * @returns {Promise<string>} - The full path to the temp file
	 */
	protected saveToTempDir(stream: ReadableStream, filename: string): Promise<string> {
		return new Promise<string>((resolve, reject) => {
			const dest: string = path.join(this.options.tmpDir, filename);
			stream.pipe(fs.createWriteStream(dest))
				.on('close', () => resolve(dest))
				.on('error', (err: Error) => reject(err));
		});
	}

	/**
	 * Removes the file from the temp directory
	 *
	 * @param filename {string} - Filename to remove
	 * @returns {Promise<void>}
	 */
	public cleanupTempFile(filename: string): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			const dest: string = path.join(this.options.tmpDir, filename);

			fs.unlink(dest, (err: Error) => {
				if (err) {
					reject(err);
				}
				else {
					resolve();
				}
			});
		});
	}
}
