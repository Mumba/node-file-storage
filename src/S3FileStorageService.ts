/**
 * S3FileStorageService.
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as fs from 'fs';
import {S3} from 'aws-sdk';
import {FileStorageService} from './FileStorageService';
import ReadableStream = NodeJS.ReadableStream;
import WritableStream = NodeJS.WritableStream;
import ReadWriteStream = NodeJS.ReadWriteStream;
import {AbstractFileStorageService, AbstractFileStorageServiceOptions} from './AbstractFileStorageService';
import PutObjectRequest = S3.Types.PutObjectRequest;
import CopyObjectRequest = S3.Types.CopyObjectRequest;
import DeleteObjectRequest = S3.Types.DeleteObjectRequest;

/**
 * Interface for configuration options
 */
export interface S3FileStorageServiceOptions extends AbstractFileStorageServiceOptions {
	Bucket: string;
	ACL?: string;
	ServerSideEncryption?: string;
	StorageClass?: string;
}

export class S3FileStorageService extends AbstractFileStorageService implements FileStorageService {

	constructor(protected options: S3FileStorageServiceOptions = <any>{}, private awsS3: S3) {
		super(options);

		if (!options.Bucket) {
			throw new Error('S3Service: <options.Bucket> required.');
		}

		if (!awsS3) {
			throw new Error('S3Service: <awsS3> required.');
		}
	}

	/**
	 * Gets a file from S3 and saves it to a temp dir
	 *
	 * @param key {string} - The AWS S3 key to the file
	 * @returns {Promise<string>} - The file path to the temp file
	 */
	public getFile(key: string): Promise<string> {
		let params: any = {
			Bucket: this.options.Bucket,
			Key: key
		};

		const stream: ReadableStream = this.awsS3.getObject(params).createReadStream();

		return this.saveToTempDir(stream, key.replace(/\//g, '_'));
	}

	/**
	 * Puts a file in the S3 bucket
	 *
	 * @param tempFilePath {string} - The path of the temp file to save
	 * @param key {string} - The AWS S3 key to save the file as
	 * @returns {Promise<void>}
	 */
	public putFile(tempFilePath: string, key: string): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			const params: PutObjectRequest = {
				Bucket: this.options.Bucket,
				Key: key,
				Body: <any>fs.createReadStream(tempFilePath),
				ACL: this.options.ACL,
				ServerSideEncryption: this.options.ServerSideEncryption,
				StorageClass: this.options.StorageClass
			};
			this.awsS3.upload(params, (err: Error) => {
				if (err) {
					return reject(err);
				}

				resolve();
			});
		});
	}

	/**
	 * Moves an existing file in the S3 bucket
	 *
	 * @param originalFilePath {string} - The path of the original file to move.
	 * @param key {string}              - The AWS S3 key to move the file to.
	 * @returns {Promise<void>}
	 */
	public moveFile(originalFilePath: string, key: string): Promise<void> {
		return new Promise<void>((resolve, reject) => {
			const copyParams: CopyObjectRequest = {
				Bucket: this.options.Bucket,
				CopySource: this.options.Bucket + '/' + originalFilePath,
				ACL: this.options.ACL,
				ServerSideEncryption: this.options.ServerSideEncryption,
				StorageClass: this.options.StorageClass,
				Key: key
			};
			const deleteParams: DeleteObjectRequest = {
				Bucket: this.options.Bucket,
				Key: originalFilePath
			};

			this.awsS3.copyObject(copyParams, (err: Error) => {
				if (err) {
					return reject(err);
				}

				this.awsS3.deleteObject(deleteParams, (err: Error) => {
					if (err) {
						return reject(err);
					}

					resolve();
				});
			});
		});
	}

	/**
	 * Get a signed URL to download a file.
	 *
	 * @param filePath
	 * @returns {string}
	 */
	public getSignedUrl(filePath: string): string {
		return this.awsS3.getSignedUrl('getObject', {
			Bucket: this.options.Bucket,
			Key: filePath
		});
	}
}
